import 'package:flutter/material.dart';
import 'package:midterm/ui/Thongbao.dart';
import 'package:midterm/ui/Thongtinlienlac.dart';
import 'package:midterm/ui/cuahang.dart';
import 'package:midterm/ui/sanpham.dart';
import 'package:midterm/ui/taikhoan.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: AppHomePage(),
    );
  }
}

class AppHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AppHomePageState();
  }
}

class AppHomePageState extends State<AppHomePage> {
  int currentIndex = 0;
  void changeIndexedStack(index) {
    setState(() {
      currentIndex = index;
    });
  }

  late List<Widget> screens;
  @override
  void initState() {
    screens = [store(), sanpham(), Thongbao(), Thongtinlienlac()];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        //-> switch giua cac trang
        index: currentIndex,
        children: screens,
      ),
      bottomNavigationBar: BottomNavigationBar(
          //=> bottom navigation bar
          type: BottomNavigationBarType.fixed,
          iconSize: 35,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.orange,
          onTap: (index) {
            setState(() {
              currentIndex = index;
            });
          },
          currentIndex: currentIndex,
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: "Cửa hàng",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.remove_red_eye),
              label: "Tổng sản phẩm",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.notification_add),
              label: "Thông báo",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: "Thông tin liên lạc",
            ),
          ]),
    );
  }
}
