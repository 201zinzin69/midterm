import 'package:flutter/material.dart';
import 'package:midterm/ui/sanpham.dart';

class taikhoan extends StatefulWidget {
  const taikhoan(
      {Key? key, required void Function(dynamic index) changeIndexedStack})
      : super(key: key);

  @override
  State<taikhoan> createState() => _taikhoanState();
}

class _taikhoanState extends State<taikhoan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        centerTitle: true,
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Đăng Nhập",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 28,
                  fontWeight: FontWeight.bold),
            ),
            Form(
              child: Column(
                children: [
                  emailField(),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                  ),
                  passwordField(),
                  Container(
                    margin: const EdgeInsets.only(top: 40.0),
                  ),
                ],
              ),
            ),

            // GoToSignUpButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        // icon: const Icon(Icons.person),
        hintText: "Email",
        // errorText: emailErrorText,
      ),
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        // icon: const Icon(Icons.password),
        // labelText: 'Password',
        hintText: "Mật Khẩu",
        // errorText: passwordErrorText),
      ),
      // validator: validatePassword,
    );
  }

  Widget SignInButton() {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color?>(Colors.orange[400]),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              //side: BorderSide(color: greyTweet, width: 0.5)
            ))),
        onPressed: () {},
        child: const Text('Sign in'));
  }

  Widget GoToSignUpButton() {
    return ElevatedButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => const sanpham(),
            ),
          );
        },
        child: const Text("Create account"));
  }
}
