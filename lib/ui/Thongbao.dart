import 'package:flutter/material.dart';
import 'package:midterm/ui/giohang/giohang.dart';

class Thongbao extends StatefulWidget {
  const Thongbao({Key? key}) : super(key: key);

  @override
  State<Thongbao> createState() => _ThongbaoState();
}

class _ThongbaoState extends State<Thongbao> {
  @override
  List<String> listsearch = [
    'UPDATE HÀNG 26/04',
    'UPDATE HÀNG 24/02',
    'KHAI XUÂN ĐẦU NĂM 2022, GIẢM GIÁ 20%',
    'HAPPY NEW YEAR 2022',
    'UPDATE 14/01/2022',
    'NOEL NĂM NAY CÓ GÌ HOTT',
    'KHUYẾN MÃI LỚN 25%',
    'FLASHSALE 11/11 CÙNG BÁN SỈ',
    'UPDATE HÀNG 1/11/2021',
    'ÁO MỚI CHO BẠN',
    'TIEDIE ĐÃ VỀ',
    'CHÀO MỪNG ĐẾN VỚI BÁN SỈ',
    'UPDATE HÀNG 17/06/2021',
    'ÁO XINH CHO BÉ',
    'PHỤ KIỆN BÔNG TAI XINH YÊU 17/04/2021',
    'VỚ TẤT FLASHSALE',
    'ĐỒNG GIÁ 100K CHO TOÀN BỘ ÁO',
    'VOUCHER GIẢM 300K',
    'TẢI BÁN SỈ NHẬN VOUCHER LIỀN TAY',
    'UPDATE HÀNG 2/02/2021',
    'FLASHSALE THÁNG 1',
    'ĐỒNG LOẠT GIẢM GIÁ 25%',
  ];
  List<String>? ListSearch;
  TextEditingController Name = TextEditingController();
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text('Chuyên bán sỉ'),
            backgroundColor: Colors.black,
            actions: [
              IconButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => giohang()));
                  },
                  icon: const Icon(Icons.shopping_bag)),
            ]),
        body: Name.text.isNotEmpty && ListSearch!.length == 0
            ? Center()
            : ListView.builder(
                itemCount: Name.text.isNotEmpty
                    ? ListSearch!.length
                    : listsearch.length,
                itemBuilder: (ctx, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        CircleAvatar(
                          child: Icon(Icons.notifications),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(Name.text.isNotEmpty
                            ? ListSearch![index]
                            : listsearch[index]),
                      ],
                    ),
                  );
                }));
  }
}
