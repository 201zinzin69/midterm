import 'package:flutter/material.dart';
import 'package:midterm/ui/giohang/giohang.dart';

class Thongtinlienlac extends StatefulWidget {
  const Thongtinlienlac({Key? key}) : super(key: key);

  @override
  State<Thongtinlienlac> createState() => _ThongtinlienlacState();
}

class _ThongtinlienlacState extends State<Thongtinlienlac> {
  TextEditingController Name = TextEditingController();
  @override
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('Chuyên bán sỉ'),
          backgroundColor: Colors.black,
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => giohang()));
                },
                icon: const Icon(Icons.shopping_bag)),
          ]),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Searh(),
                Thongtin(),
                Thongtin2(),
                Thongtin3(),
                Thongtin4(),
                Thongtin5()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget Searh() {
    return Container(
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32),
                  borderSide: BorderSide.none),
              filled: true,
              fillColor: Color(0xFFF5F5F5),
              hintStyle: TextStyle(color: Color(0xFF959595)),
              hintText: "Tìm kiếm",
              suffixIcon: Icon(Icons.search),
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 30.0),
          ),
        ],
      ),
    );
  }

  Widget Thongtin() {
    return Container(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://i.insider.com/56e34aad52bcd022008b5fa5?width=1000&format=jpeg&auto=webp'),
                ),
                borderRadius: BorderRadius.circular(12),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Đàm Việt Cường ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "0923456789 ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Thongtin2() {
    return Container(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://www.thelist.com/img/gallery/the-most-dramatic-celebrity-transformations-of-the-past-decade/intro-1575646375.jpg'),
                ),
                borderRadius: BorderRadius.circular(12),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Đoàn Văn Nghĩa ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "0998765432 ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Thongtin3() {
    return Container(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://ichef.bbci.co.uk/news/976/cpsprodpb/C26C/production/_111927794_gettyimages-1192169655.jpg'),
                ),
                borderRadius: BorderRadius.circular(12),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Mai Đạt ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "09456589052 ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Thongtin4() {
    return Container(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://img.cdnki.com/celebrity-nghia-la-gi---2b9d76ac9d5ff0c4fb6398df46016d5a.wepb'),
                ),
                borderRadius: BorderRadius.circular(12),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Hoàng Trung Đức ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "09123435346 ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Thongtin5() {
    return Container(
      child: Card(
        elevation: 0,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://mymodernmet.com/wp/wp-content/uploads/archive/9YPBjDyXBmK6zd25PAM1_gesichtermix14.jpg'),
                ),
                borderRadius: BorderRadius.circular(12),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Trần Hoài Phát",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "0912356889 ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
