import 'package:flutter/material.dart';

import '../ui/sign_in.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return SignUpScreenState();
  }
}

class SignUpScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;
  // final bloc = Bloc();
  String emailValidate = "";
  late String emailErrorText = "";
  late String passwordErrorText = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        centerTitle: true,
        title: const Image(
          image: AssetImage('assets/logo.png'),
          width: 27,
          height: 27,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Create your account",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 28,
                  fontWeight: FontWeight.bold),
            ),
            Form(
              key: formKey,
              child: Column(
                children: [
                  emailField(),
                  Container(
                    margin: const EdgeInsets.only(top: 20.0),
                  ),
                  passwordField(),
                  Container(
                    margin: const EdgeInsets.only(top: 40.0),
                  ),
                ],
              ),
            ),
            const Spacer(),
            const Divider(),
            Row(
              children: [
                Spacer(),
                SignUpButton(),
              ],
            ),
            // GoToSignUpButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        // icon: const Icon(Icons.person),
        hintText: "Email",
        // errorText: emailErrorText,
      ),
      // validator: validateEmail,
      onChanged: (value) {
        // print(value);
        // bloc.changedEmail(value);
        // if (!value.contains("@")) {
        //   emailValidate = "invalid email";
        // } else {
        //   emailValidate = "";
        // }
        setState(() {
          emailErrorText = "";
        });
      },
      onSaved: (value) {
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        // icon: const Icon(Icons.password),
        // labelText: 'Password',
        hintText: "Password",
        // errorText: passwordErrorText),
      ),
      // validator: validatePassword,
      onSaved: (value) {
        password = value as String;
      },
      // onChanged: (value) {
      //   setState(() {
      //     emailErrorText = "";
      //   });
      // },
    );
  }

  // Widget DateField() {
  //   return TextFormField(
  //     obscureText: true,
  //     keyboardType: TextInputType.number,
  //     inputFormatters: <TextInputFormatter>[
  //       FilteringTextInputFormatter.digitsOnly,
  //     ],
  //     decoration: InputDecoration(
  //         icon: Icon(Icons.calendar_today), labelText: 'Year of birth'),
  //     validator: (value) {
  //       if (value!.length < 4) {
  //         return "Years has at least 4 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  // Widget NameField() {
  //   return TextFormField(
  //     obscureText: true,
  //     decoration:
  //         InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
  //     validator: (value) {
  //       if (value!.length < 1) {
  //         return "Name has at least 1 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  // Widget AddressField() {
  //   return TextFormField(
  //     decoration: InputDecoration(
  //         icon: Icon(Icons.location_city_sharp), labelText: "Address"),
  //     validator: validateAddress,
  //     onSaved: (value) {
  //       print('onSaved: value=$value');
  //     },
  //   );
  // }

  // Widget PasswordField() {
  //   return TextFormField(
  //     obscureText: true,
  //     decoration:
  //         InputDecoration(icon: Icon(Icons.person), labelText: 'Full name'),
  //     validator: (value) {
  //       if (value!.length < 1) {
  //         return "Name has at least 1 characters.";
  //       }

  //       return null;
  //     },
  //     onSaved: (value) {
  //       password = value as String;
  //     },
  //   );
  // }

  Widget SignUpButton() {
    return ElevatedButton(
        style: ButtonStyle(
            backgroundColor:
                MaterialStateProperty.all<Color?>(Colors.orange[400]),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(30.0),
              //side: BorderSide(color: greyTweet, width: 0.5)
            ))),
        onPressed: () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => SignInScreen()));
        },
        child: const Text('Create account'));
  }

  Widget GoToSignUpButton() {
    return ElevatedButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => const SignUpScreen(),
            ),
          );
        },
        child: const Text("Create account"));
  }
}
