import 'package:flutter/material.dart';
import 'package:flutter_elegant_number_button/flutter_elegant_number_button.dart';

import 'package:page_transition/page_transition.dart';

class giohang extends StatefulWidget {
  @override
  State<giohang> createState() => _giohangState();
}

class _giohangState extends State<giohang> {
  @override
  num quantity = 1;
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Giỏ Hàng',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        foregroundColor: Colors.white,
        backgroundColor: Colors.black,
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Address(),
                Productd_details(),
                Soldby(),
                Product(),
                Price(),
                TotalPrice(),
                Shippingdetails(),
                Shippingfee(),
                Totalfee(),
                Submit()
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget Address() {
    return Container(
      color: Color.fromARGB(255, 244, 234, 234),
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                width: 500,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                ),
                child: ListTile(
                  title: Text('Thêm địa chỉ',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontWeight: FontWeight.bold)),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Productd_details() {
    return Container(
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Sản phẩm',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Soldby() {
    return Container(
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                width: 500,
                color: Colors.white,
                child: ListTile(
                  title: Text('Bán bởi: Trần Hoài Phát ',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Product() {
    return Container(
      child: Card(
        elevation: 5,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: ClipRRect(
                child: Image(
                  fit: BoxFit.fill,
                  image: NetworkImage(
                      'https://s.yimg.com/ny/api/res/1.2/_X57eRKHbu.OF9wDbrAsMg--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MA--/https://s.yimg.com/os/creatr-uploaded-images/2021-09/1538ed90-0b59-11ec-bffa-b35b3846a5a0'),
                ),
                borderRadius: BorderRadius.circular(20),
              )),
              Expanded(
                flex: 6,
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        //product's shop
                        padding: EdgeInsets.all(8),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                "Giày Nike đen ",
                                style: TextStyle(
                                    fontSize: 17, color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Price() {
    return Container(
      decoration: BoxDecoration(
          color: Color.fromARGB(255, 244, 234, 234),
          borderRadius: BorderRadius.circular(20)),
      height: 96,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Row(
                children: [
                  Container(
                    width: 300,
                    child: ListTile(
                      title: Text('300.000 VND',
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  Center(
                    child: ElegantNumberButton(
                      initialValue: quantity,
                      minValue: 1,
                      maxValue: 100,
                      step: 1,
                      buttonSizeHeight: 30,
                      buttonSizeWidth: 30,
                      decimalPlaces: 0,
                      color: Colors.white,
                      onChanged: (value) async {
                        setState(() {
                          quantity = value;
                        });
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget TotalPrice() {
    return Container(
      width: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text(
                    'Giá: ',
                    style: TextStyle(
                      color: Color.fromARGB(255, 159, 157, 157),
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.all(22),
            child: Text(
              "300.000 VND",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget Shippingdetails() {
    return Container(
      width: 100,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Thông tin Ship',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget Shippingfee() {
    return Container(
      width: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Phí Ship',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          Spacer(),
          Container(
              padding: EdgeInsets.all(22),
              child: Text(
                "28.000 VND",
                style: TextStyle(fontWeight: FontWeight.bold),
              ))
        ],
      ),
    );
  }

  Widget Totalfee() {
    return Container(
      width: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('Tổng',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.all(22),
            child: Text(
              "328.000 VND",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }

  Widget Submit() {
    return Container(
      width: 100,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            child: IntrinsicWidth(
              child: Container(
                color: Colors.white,
                child: ListTile(
                  title: Text('328.000 VND',
                      style: TextStyle(
                          color: Color.fromARGB(255, 159, 157, 157),
                          fontSize: 15,
                          fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          Spacer(),
          Container(
            width: 100,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  child: IntrinsicWidth(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(23),
                          color: Colors.orange),
                      child: Center(
                        child: Text(
                          "Submit",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
