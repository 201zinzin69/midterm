import 'package:flutter/material.dart';
import 'package:midterm/app.dart';
import 'package:midterm/ui/sign_in.dart';
import 'package:midterm/ui/sign_up.dart';

class StartUpScreen extends StatefulWidget {
  const StartUpScreen({Key? key}) : super(key: key);

  @override
  State<StartUpScreen> createState() => _StartUpScreenState();
}

class _StartUpScreenState extends State<StartUpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.white,
        centerTitle: true,
        title: ImageIcon(
          const AssetImage('assets/logo.png'),
          size: 27,
          color: Colors.orange,
        ),
      ),
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.only(top: 50, bottom: 20, left: 40, right: 40),
        child: Column(
          children: [
            Spacer(),
            Text(
              "See what's happening in world right now ",
              style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
            ),
            Spacer(),
            ElevatedButton(
              onPressed: () {},
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      image: AssetImage("assets/google_icon.png"),
                      width: 20,
                      height: 20,
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10),
                    ),
                    Text(
                      "Continue with Google",
                      style: TextStyle(
                          color: Colors.black, fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              ),
              style: ButtonStyle(
                  elevation: MaterialStateProperty.all<double?>(0),
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.white),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.grey, width: 0.5)))),
            ),
            Divider(),
            ElevatedButton(
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SignUpScreen(),
                  ),
                );
              },
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Create account",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.w800),
                    ),
                  ],
                ),
              ),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color?>(Colors.orange),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    //side: BorderSide(color: greyTweet, width: 0.5)
                  ))),
            ),
            Spacer(),
            Row(
              children: [
                Text("Have an account already?",
                    style: TextStyle(
                        fontSize: 15,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500)),
                TextButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => SignInScreen(),
                      ),
                    );
                  },
                  child: Text("Login",
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.orange,
                          fontWeight: FontWeight.w500)),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
