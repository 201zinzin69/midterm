import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:midterm/ui/giohang/giohang.dart';
import 'package:midterm/ui/sanpham.dart';
import 'package:transparent_image/transparent_image.dart';

class store extends StatefulWidget {
  @override
  State<store> createState() => _storeState();
}

List<String> imageNikeList = [
  'https://s.yimg.com/ny/api/res/1.2/_X57eRKHbu.OF9wDbrAsMg--/YXBwaWQ9aGlnaGxhbmRlcjt3PTY0MA--/https://s.yimg.com/os/creatr-uploaded-images/2021-09/1538ed90-0b59-11ec-bffa-b35b3846a5a0',
  'https://static.nike.com/a/images/t_PDP_1280_v1/f_auto,q_auto:eco/u7khoqev6hy2xgsllrnb/revolution-5-road-running-shoes-szF7CS.png',
  'https://cdn11.bigcommerce.com/s-mhohx7q5do/images/stencil/original/carousel/54/Best-Vans-Shoes-walking.png?c=1',
  'https://images.lululemon.com/is/image/lululemon/Web_LW9EF1S_055218_0075',
  'https://i.insider.com/5ebf0f752618b91d132c2c40?width=1200&format=jpeg',
  'https://pyxis.nymag.com/v1/imgs/a98/d0a/ad37aae9d281b562d1afe26fdc8a28cbd6.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBeu-9USQZeyTY4WvWxLvHRfe_F7ztMhdQkg&usqp=CAU',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREO1bk2C46N5SrIWLPA8WLQdyrM5AVUfR5QA&usqp=CAU',
  'https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10339033/2022/4/25/3170e623-ab80-4678-9628-14cb6033ab171650883660619USPoloAssnMenWhiteClarkinSneakers1.jpg',
];

List<String> imageVanList = [
  'https://bizweb.dktcdn.net/thumb/1024x1024/100/373/032/products/vans-old-skool-black-white-ship-us.jpg?v=1601636632237',
  'https://cafedidong.vn/wp-content/uploads/2019/11/CAFEDIDONG-Vans-caro-2X-0.jpg',
  'https://cdn11.bigcommerce.com/s-mhohx7q5do/images/stencil/original/carousel/54/Best-Vans-Shoes-walking.png?c=1',
  'https://drake.vn/image/catalog/H%C3%ACnh%20content/Giay-Vans-chinh-hang-TPHCM/giay-vans-chinh-hang-tphcm-08.jpg',
  'https://giaysneakerhcm.com/wp-content/uploads/2019/08/giay-vans-marvel-replica-11-1-750x500.jpg',
  'https://allimages.sgp1.digitaloceanspaces.com/pedrovietnamcom/2021/06/1623931674_378_Top-10-Mau-Giay-Vans-Dep-2020-2021-Khong-The.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBeu-9USQZeyTY4WvWxLvHRfe_F7ztMhdQkg&usqp=CAU',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREO1bk2C46N5SrIWLPA8WLQdyrM5AVUfR5QA&usqp=CAU',
  'https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10339033/2022/4/25/3170e623-ab80-4678-9628-14cb6033ab171650883660619USPoloAssnMenWhiteClarkinSneakers1.jpg',
];

List<String> imageConverseList = [
  'https://bizweb.dktcdn.net/thumb/1024x1024/100/373/032/products/vans-old-skool-black-white-ship-us.jpg?v=1601636632237',
  'https://cafedidong.vn/wp-content/uploads/2019/11/CAFEDIDONG-Vans-caro-2X-0.jpg',
  'https://cdn11.bigcommerce.com/s-mhohx7q5do/images/stencil/original/carousel/54/Best-Vans-Shoes-walking.png?c=1',
  'https://drake.vn/image/catalog/H%C3%ACnh%20content/Giay-Vans-chinh-hang-TPHCM/giay-vans-chinh-hang-tphcm-08.jpg',
  'https://giaysneakerhcm.com/wp-content/uploads/2019/08/giay-vans-marvel-replica-11-1-750x500.jpg',
  'https://allimages.sgp1.digitaloceanspaces.com/pedrovietnamcom/2021/06/1623931674_378_Top-10-Mau-Giay-Vans-Dep-2020-2021-Khong-The.jpg',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBeu-9USQZeyTY4WvWxLvHRfe_F7ztMhdQkg&usqp=CAU',
  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREO1bk2C46N5SrIWLPA8WLQdyrM5AVUfR5QA&usqp=CAU',
  'https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10339033/2022/4/25/3170e623-ab80-4678-9628-14cb6033ab171650883660619USPoloAssnMenWhiteClarkinSneakers1.jpg',
];

class _storeState extends State<store> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          title: Text('Chuyên bán sỉ'),
          backgroundColor: Colors.black,
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => giohang()));
                },
                icon: const Icon(Icons.shopping_bag)),
          ]),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(bottom: 15),
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(),
                    TextField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32),
                            borderSide: BorderSide.none),
                        filled: true,
                        fillColor: Color(0xFFF5F5F5),
                        hintStyle: TextStyle(color: Color(0xFF959595)),
                        hintText: "Tìm kiếm",
                        suffixIcon: Icon(Icons.search),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(bottom: 30.0),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Giày Nike",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 1000,
                  child: StaggeredGridView.countBuilder(
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 12,
                      itemCount: 12,
                      itemBuilder: (context, index) {
                        return Container(
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: imageNikeList[index],
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                      staggeredTileBuilder: (index) {
                        return StaggeredTile.count(1, index.isEven ? 1.2 : 1.8);
                      }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Giày Vans",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 1000,
                  child: StaggeredGridView.countBuilder(
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 12,
                      itemCount: 12,
                      itemBuilder: (context, index) {
                        return Container(
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: imageVanList[index],
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                      staggeredTileBuilder: (index) {
                        return StaggeredTile.count(1, index.isEven ? 1.2 : 1.8);
                      }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    "Giày Converse",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 1000,
                  child: StaggeredGridView.countBuilder(
                      physics: NeverScrollableScrollPhysics(),
                      crossAxisCount: 2,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 12,
                      itemCount: 12,
                      itemBuilder: (context, index) {
                        return Container(
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(15))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            child: FadeInImage.memoryNetwork(
                              placeholder: kTransparentImage,
                              image: imageConverseList[index],
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      },
                      staggeredTileBuilder: (index) {
                        return StaggeredTile.count(1, index.isEven ? 1.2 : 1.8);
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
