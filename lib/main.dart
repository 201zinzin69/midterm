import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:midterm/ui/startup.dart';

import 'app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: Colors.transparent,
  ));
  runApp(MaterialApp(home: StartUpScreen()));
}
